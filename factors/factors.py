#debug state, make true to enable debug output and pause points
debug = False

def factor(x):
	start = x	#store original x
	place = 2	#what number x is currently being tested against
	factors = []	#list that will contain factors of x or remain empty in x is prime

	while(place <= (x)): #while test is still less then remainder
		
		if(debug):  print(str(place) + ", " + str(x) + ", " + str(factors))	#debug output
		if(debug):  input(" ")							#debug pause point
		
		if(x%place==0):			#if test is evenly divisible into x
			factors.append(place)	#add valuse to factor list
			x = int(x/place)	#divide out of x
		else:				#else increment place varible
			place += 1
	return factors if len(factors) > 1 else []

ans = factor(int(input("what integer should I try and factor? ")))
	
print(ans)
